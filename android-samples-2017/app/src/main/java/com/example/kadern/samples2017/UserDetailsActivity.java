package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid";
    private static String VALIDATE_MESSAGE = "User is not valid:";

    //user and form instance variables
    User user;
    EditText txtFirstName;
    EditText txtEmail;
    RadioGroup rgFavoriteMusic;
    CheckBox chkActive;
    Button btnSave;
    //clear the form
    Button btnClearForm;
    //setting up the database connections
    UserDetailsHelper dbHelper;
    UserDataAccess da;

    //onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        //setting up a new intent for the userId
        Intent i = getIntent();
        final Long userId = i.getLongExtra(USER_ID_EXTRA,-1);
        //database variables
        dbHelper = new UserDetailsHelper(this);
        da = new UserDataAccess(dbHelper);

        //form variables
        txtFirstName = (EditText)findViewById(R.id.txtFirstName);
        txtEmail = (EditText)findViewById(R.id.txtEmail);
        rgFavoriteMusic = (RadioGroup)findViewById(R.id.rgFavoriteMusic);
        chkActive = (CheckBox)findViewById(R.id.chkActive);
        btnSave = (Button)findViewById(R.id.btnSave);

        //if there is a user
        if(userId >= 0){
            //getting the user data by the userId
            user = da.getUserById(userId);
            //putting the user data inside the form
            putDataInUI();
        }

        //saves the user in the database
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //gets the user data from the form
                getDataFromUI();

                //checks validation
                boolean valid = validation();
                if(valid == true){
                    //if the user has an id update user else insert user
                    if (userId > 0){
                        da.updateUser(user);
                    } else {
                        da.insertUser(user);
                    }
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                } else if (valid == false) {
                    validateDialog();
                } else {
                    Toast.makeText(UserDetailsActivity.this, "Validation Error", Toast.LENGTH_LONG).show();
                }
            }
        });
        //clear form button
        btnClearForm = (Button)findViewById(R.id.btnClearForm);
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetForm();
            }
        });
    }

    //gets the values of the form inputs and sets them to the user
    private void getDataFromUI(){

        if(user == null){
            user = new User();
        }
        String firstName = txtFirstName.getText().toString();
        String email = txtEmail.getText().toString();
        Boolean active = chkActive.isChecked();
        // Here's how we can set the favorite music...
        User.Music favoriteMusic = null;
        int selectedRadioButtonId = rgFavoriteMusic.getCheckedRadioButtonId();

        switch(selectedRadioButtonId){
            case R.id.rbCountry:
                favoriteMusic = User.Music.COUNTRY;
                break;
            case R.id.rbRap:
                favoriteMusic = User.Music.RAP;
                break;
            case R.id.rbJazz:
                favoriteMusic = User.Music.JAZZ;
                break;
        }

        //setting the user to the values of the inputs
        user.setFirstName(firstName);
        user.setEmail(email);
        user.setActive(active);
        user.setFavoriteMusic(favoriteMusic);
    }

    //puts the user information from the UserListActivity.java list
    private void putDataInUI(){

        txtFirstName.setText(user.getFirstName());
        txtEmail.setText(user.getEmail());
        chkActive.setChecked(user.isActive());

        switch(user.getFavoriteMusic()){
            case COUNTRY:
                rgFavoriteMusic.check(R.id.rbCountry);
                break;
            case JAZZ:
                rgFavoriteMusic.check(R.id.rbJazz);
                break;
            case RAP:
                rgFavoriteMusic.check(R.id.rbRap);
                break;
        }
    }

    //reset the form, used in the btnClearForm
    private void resetForm(){
        rgFavoriteMusic.clearCheck();
        txtFirstName.setText("");
        txtEmail.setText("");
        chkActive.setChecked(false);
    }

    // validation method
    // Return true if valid
    // Return false if not valid
    private Boolean validation(){
        Integer valid = 1;
        //validating the email text to see if email
        Boolean bolEmail = isEmailValid(user.getEmail());
        if (bolEmail == false){
            txtEmail.setError("Invalid Email");
            VALIDATE_MESSAGE += "\nInvalid Email.";
            valid = 0;
        }
        //validating if there is a firstName
        if (user.getFirstName().equals("")){
            txtFirstName.setError("Invalid Name");
            VALIDATE_MESSAGE += "\nInvalid Name.";
            valid = 0;
        }
        //validate if there is a FavoriteMusic
        if (user.getFavoriteMusic() == null){
            valid = 0;
            VALIDATE_MESSAGE += "\nNo music selected.";
        }

        if(valid == 1){
            return true;
        } else {
            return false;
        }
    }

    //checking if email is valid
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //dialog if not valid
    private void validateDialog(){
        new AlertDialog.Builder(UserDetailsActivity.this)
                .setTitle("Not Valid")
                .setMessage(VALIDATE_MESSAGE)
                .setNeutralButton(android.R.string.ok, null)
                .create()
                .show();

    }
}