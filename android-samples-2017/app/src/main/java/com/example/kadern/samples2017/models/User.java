package com.example.kadern.samples2017.models;

import java.io.Serializable;


/**
 * Created by kadern on 9/18/2017.
 */

public class User implements Serializable {
    public enum Music{COUNTRY, RAP, JAZZ};

    private long id;
    private String firstName;
    private String email;
    private Music favoriteMusic;
    private Boolean active;

    public User(){

    }

    public User(long id, String firstName, String email, Music favoriteMusic, Boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString(){
        return String.format("ID: %d FName: %s Email: %s Music: %s Active: %b", id, firstName, email, favoriteMusic, active);

    }

}
