package com.example.kadern.samples2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class IntentReceiverActivity extends AppCompatActivity {

    public static final String MY_TEXT_DATA = "mytextdata";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_receiver);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MY_TEXT_DATA);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
