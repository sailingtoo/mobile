package com.example.kadern.samples2017;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.kadern.samples2017.dataaccess.UserDataAccess;
import com.example.kadern.samples2017.models.User;

public class UserListActivity extends AppCompatActivity {

    //setting up the instance variables
    //app class and DataAccess class
    AppClass app;
    ListView userListView;
    Button btnAddNewUser;
    UserDetailsHelper dbHelper;
    UserDataAccess da;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        //setting up the view
        btnAddNewUser = (Button)findViewById(R.id.btnAddNewUser);
        btnAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(UserListActivity.this, UserDetailsActivity.class));
            }
        });
        app = (AppClass)getApplication();
        userListView = (ListView)findViewById(R.id.userListView);
        dbHelper = new UserDetailsHelper(this);

        //getting the database information
        da = new UserDataAccess(dbHelper);
        app.users = da.getAllUsers();

        ArrayAdapter adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, app.users){

            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View listItemView = super.getView(position, convertView, parent);
                User currentUser = app.users.get(position);

                TextView lbl = (TextView)listItemView.findViewById(R.id.lblFirstName);
                Button btnEditUser = (Button)listItemView.findViewById(R.id.btnEditUser);
                Button btnDeleteUser = (Button)listItemView.findViewById(R.id.btnDeleteUser);

                btnEditUser.setTag(currentUser);
                btnDeleteUser.setTag(currentUser);

                lbl.setText(currentUser.getFirstName());
                //edit the user
                btnEditUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        User selectedUser = (User)view.getTag();
                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(UserDetailsActivity.USER_ID_EXTRA, selectedUser.getId());
                        startActivity(i);
                    }
                });
                //delete the user
                btnDeleteUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        User selectedUser = (User)view.getTag();
                        deleteUser(selectedUser);

                    }
                });
                return listItemView;
            }
        };
        userListView.setAdapter(adapter);
    }

    // delete the user from the database dialog
    // positive starts new UserListActivity with the deleted change
    // negative returns null
    private void deleteUser(final User user){
        new AlertDialog.Builder(UserListActivity.this)
                .setTitle("Delete User")
                .setMessage("Are you sure you want to delete the user?")
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        da.deletedUser(user);
                        startActivity(new Intent(UserListActivity.this, UserListActivity.class));
                    }
                })
                .create()
                .show();
    }
}
