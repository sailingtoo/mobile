package com.example.kadern.samples2017.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.kadern.samples2017.UserDetailsHelper;
import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

/**
 * Created by 003012468 on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";
    private UserDetailsHelper dbHelper;
    private SQLiteDatabase database;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_FIRST_NAME = "user_first_name";
    public static final String COLUMN_USER_EMAIL = "user_email";
    public static final String COLUMN_USER_FAVORITE_MUSIC = "user_music";
    public static final String COLUMN_USER_ACTIVE = "user_active";

    //creating the table
    public static final String TABLE_CREATE =
            String.format("create table %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
                    TABLE_NAME,
                    COLUMN_USER_ID,
                    COLUMN_USER_FIRST_NAME,
                    COLUMN_USER_EMAIL,
                    COLUMN_USER_FAVORITE_MUSIC,
                    COLUMN_USER_ACTIVE
            );

    //User data access
    public UserDataAccess(UserDetailsHelper dbHelper){
        this.dbHelper = dbHelper;
        this.database = this.dbHelper.getWritableDatabase();
    }
    //insert user
    public User insertUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_FIRST_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());

        long insertId = database.insert(TABLE_NAME, null, values);

        u.setId(insertId);
        return u;
    }
    //get all users
    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s",
                COLUMN_USER_ID,
                COLUMN_USER_FIRST_NAME,
                COLUMN_USER_EMAIL,
                COLUMN_USER_FAVORITE_MUSIC,
                COLUMN_USER_ACTIVE,
                TABLE_NAME
        );

        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            User user = new User();
            user.setId(c.getLong(0));
            user.setFirstName(c.getString(1));
            user.setEmail(c.getString(2));
            //changing the database data-type String to Music
            User.Music um = null;
            String str = c.getString(3);
            if(str.equals("JAZZ")){
                um = User.Music.JAZZ;
            } else if(str.equals("RAP")){
                um = User.Music.RAP;
            } else if (str.equals("COUNTRY")){
                um = User.Music.COUNTRY;
            } else {

            }
            user.setFavoriteMusic(um);
            user.setActive(c.getString(4).equalsIgnoreCase("true") ? true : false);

            users.add(user);
            c.moveToNext();
        }
        c.close();
        return users;
    }

    //update Users
    public User updateUser(User u){
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_FIRST_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, u.isActive());

        int rowsUpdated = database.update(TABLE_NAME, values, "_id" + " = " + u.getId(), null);

        return u;
    }

    //deleting User
    public int deletedUser(User u){
        int rowsDeleted = database.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);

        return rowsDeleted;
    }

    //get user by Id
    public User getUserById(Long userId) {
        User user = new User();

        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s WHERE %s = %s",
                COLUMN_USER_ID,
                COLUMN_USER_FIRST_NAME,
                COLUMN_USER_EMAIL,
                COLUMN_USER_FAVORITE_MUSIC,
                COLUMN_USER_ACTIVE,
                TABLE_NAME,
                COLUMN_USER_ID,
                userId
        );
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();

        user.setId(c.getLong(0));
        user.setFirstName(c.getString(1));
        user.setEmail(c.getString(2));
        //changing the database data-type String to Music
        User.Music um = null;
        String str = c.getString(3);
        if(str.equals("JAZZ")){
            um = User.Music.JAZZ;
        } else if(str.equals("RAP")){
            um = User.Music.RAP;
        } else if (str.equals("COUNTRY")){
            um = User.Music.COUNTRY;
        } else {
        }

        user.setFavoriteMusic(um);
        user.setActive(c.getString(4).equalsIgnoreCase("1") ? true : false);

        c.close();

        return user;
    }
}
